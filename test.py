from src.main import greeting

print("What are you doing here?")
print("Oh apologies, where I my manners")
repents = 0

while True:
    try:
        repents = int(input("How many times should I repent?" ))
    except ValueError:
        print("Please, enter a valid integer")
        continue
    else:
        greeting(repents)
        break
